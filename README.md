# Fake Data Generator

Fake test data is often needed for testing security and anomaly detection applications. This program uses the python `faker` module to generate fake data and upload it to AWS S3. The program is meant to be deployed as a lambda serverless function and invoked based on a cloudwatch schedule. 

Current the following Fake data is generated

* SSN with Names
* Credit Card numbers with Name and Address
* ITIN numbers with names
* Addresses with names
* AWS credentails 

The code is provided "AS-IS", please review it carefully and use responsibly.  

## Usage

* Clone the repo and install the dependencies in `requirements.txt`
* Update with a S3 bucket or pass it as a env variable
* Bundle the deployment package for lambda and deploy in a python 3.6 environment
* Invoke the lambda using cloudwatch events
* Setup a lifecycle policy on your bucket to expire the generated objects 

#ToDo
* Add additional providers
 
