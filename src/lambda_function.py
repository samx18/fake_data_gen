import botocore
import boto3
from faker import Factory
from faker import Faker
from faker.providers import BaseProvider
import uuid
import random
import string


bucket = 'YOURTESTBUCKET'

"""
Create classes for custom providers
"""
class FakeIdentifiers(BaseProvider):
    def itin(self):
        """
        Create a fake Individual Taxpayer Identification Number (ITIN)
        """

        return '9%02d-7%d-%04d' % (
            random.randint(0, 99), random.randint(0, 9),
            random.randint(0, 9999))


class FakeCredentials(BaseProvider):
    BASE64_CHARS = string.ascii_letters + string.digits + '+/'
    def aws_creds(self):
        """
        Create a fake set of temporary or long lived AWS credentials
        """
        access_key = ''.join(
            random.choice(string.ascii_uppercase)
            for x in range(16))

        secret_key = ''.join(
            random.choice(self.BASE64_CHARS)
            for x in range(40))

        token = None
        if random.random() > 0.5:
            access_key = 'ASIA' + access_key

            token = 'FQoDYXdzEJb//////////wEaD' + ''.join(
                random.choice(self.BASE64_CHARS)
                for x in range(471))
        else:
            access_key = 'AKIA' + access_key
        creds = 'aws_access_key_id=%s\r\n' % access_key
        creds += 'aws_secret_access_key=%s\r\n' % secret_key
        if token:
            creds += 'aws_session_token=%s\r\n' % token

        return creds

def create_faker():
    """
    Create a faker object and the custom fake data providers
    """

    faker = Faker()
    faker.add_provider(FakeIdentifiers)
    faker.add_provider(FakeCredentials)
    return faker

def fake_cc(fake):
    filename = "credit_card/"+uuid.uuid4().hex+".fake"
    cc_record = fake.name()+","+fake.address().replace("\n",",")+","+fake.credit_card_number(card_type='mastercard')
    s3_uploader(cc_record,filename)
    return

def fake_ssn(fake):
    filename = "ssn/"+uuid.uuid4().hex+".fake"
    ssn_record = fake.name()+" "+fake.ssn()
    s3_uploader(ssn_record,filename)
    return

def fake_address(fake):
    filename = "address/"+uuid.uuid4().hex+".fake"
    address_record = fake.name()+"\n"+fake.address()
    s3_uploader(address_record,filename)
    return

def fake_itin(fake):
    filename = "itin/"+uuid.uuid4().hex+".fake"
    itin_record = fake.name()+" "+fake.itin()
    s3_uploader(itin_record,filename)
    return
    

def fake_aws_creds(fake):
    filename = "aws_credentials/"+uuid.uuid4().hex+".fake"
    aws_record = fake.aws_creds()
    s3_uploader(aws_record,filename)
    return

def s3_uploader(body,filename):
    client = boto3.client('s3')
    """
    Convert record to binary
    """
    body = body.encode() 
    response = client.put_object(Body=body,Bucket=bucket,Key=filename)
    return response

"""
Function to generate all the fake data
"""

def data_gen(fake):
    fake_cc(fake)
    fake_ssn(fake)
    fake_address(fake)
    fake_itin(fake)
    fake_aws_creds(fake)
    return

def lambda_handler(event,context):
    
    """
    Create a faker object with the built in and custom providers
    """
    fake = create_faker()
    
    try:
        data_gen(fake)
    except Exception as e:
        print("Error: "+str(e)+"!, Exiting gracefully" )   
    
    return
